import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/*
 * possible features:
 *   - Display the location for each move in the format (col, row) in the move history list. DONE
 *   - Bold the currently selected item in the move list.
 *   - Rewrite Board to use two loops to make the squares instead of hardcoding them. DONE
 *   - Add a toggle button that lets you sort the moves in either ascending or descending order. DONE
 *   - When someone wins, highlight the three squares that caused the win. DONE
 *   - When no one wins, display a message about the result being a draw. DONE
 */

function Square (props) {
    return (
        <button style={props.style} className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    renderSquare(i, winningField) {
        let style = {};
        if(winningField)
            style.backgroundColor = 'green';
        if(this.props.lastClicked===i)
            style.color = 'black';

        return (
            <Square
                style={style}
                key={'square_'+i}
                value={this.props.squares[i]}
                onClick={() => this.props.onClick(i)}
            />
        );
    }

    handleClick(i) {
        const squares = this.state.squares.slice();
        if(calculateWinner(squares) || squares[i])
            return;
        squares[i] = this.state.xIsNext ? 'X': 'O';
        this.setState({
            squares: squares,
            xIsNext: !this.state.xIsNext
        });
    }
    renderRow(row_num){
        let col_num = row_num * 3;
        let html = [];
        for(let i = 0; i < 3; i++) {
            let index = i + col_num;
            html.push(this.renderSquare(index, this.props.winningFields.includes(index)));


        }
        let row_key = 'row' + row_num;
        return (
            <div key={row_key} className="board-row">
                {html}
            </div>
        )
    }
    renderBoard(num_rows){
        let html = [];
        for(let i = 0; i<num_rows; i++){
            html.push(this.renderRow(i));
        }
        return html;
    }


    render() {
        return (
            <div>
                {this.renderBoard(3)}
            </div>
        );
    }
}

class Game extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null)
            }],
            stepNumber: 0,
            xIsNext: true,
            asc_ordering: true
        }
    }
    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if(calculateWinner(squares) || squares[i])
            return;
        squares[i] = this.state.xIsNext && this.state.stepNumber < 9 ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
            currentStep: [] // TODO: set some state variable to determine current step
        })

    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0
        });
    }
    toggle_order(){
        this.setState({
            asc_ordering: !this.state.asc_ordering
        })
    }
    get_winning_fields(){
        return find_winning_indexes(this.state.history[this.state.stepNumber].squares)
    }
    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);
        let moves = history.map((step, move) => {
            const desc = move ?
                `Go to move #${move}` :
                'Go to game start';
            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });
        if(!this.state.asc_ordering){
            moves = moves.reverse();
        }
        let status;
        let winning_fields = [];
        if(winner) {
            status = `Winner: ${winner}`;
            winning_fields = this.get_winning_fields();
        } else if(this.state.stepNumber === 9){
            status = 'Obviously draw.'
        }
        else {
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }
        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i) => this.handleClick(i)}
                        winningFields={winning_fields}
                        lastClicked={1}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <div><button onClick={() => this.toggle_order()}>Toggle ordering.</button></div>
                    <ul>{moves}</ul>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);


function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    for(let i = 0; i<lines.length; i++){
        const [a, b, c] = lines[i];
        if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c])
            return squares[a];
    }
}

function find_winning_indexes(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    let indexes = [];
    for(let i=0; i<lines.length; i++){
        const [a, b, c] = lines[i];
        if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c])
            indexes.push(a, b, c)
    }
    return indexes;
}